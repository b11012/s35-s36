const express = require('express');
const router = express.Router();


const userControllers = require('../controllers/userControllers');

console.log(userControllers);

// create user route
router.post('/', userControllers.createUserController)

// get all user route
router.get('/', userControllers.getAllUsersController);

// // get single user
router.get('/getSingleUser/:id', userControllers.getSingleUserController);

// 
router.put('/updateUser/:id', userControllers.updateUserStatusController);

module.exports = router;
